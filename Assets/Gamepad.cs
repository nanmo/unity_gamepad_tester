﻿using UnityEngine;
using System.Collections;

public class Gamepad : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI () {
		GUILayout.Label(AxisLabel("X axis"));
		GUILayout.Label(AxisLabel("Y axis"));
		GUILayout.Label(AxisLabel("3rd axis"));
		GUILayout.Label(AxisLabel("4th axis"));
		GUILayout.Label(AxisLabel("5th axis"));
		GUILayout.Label(AxisLabel("6th axis"));
		GUILayout.Label(AxisLabel("7th axis"));
		GUILayout.Label(AxisLabel("8th axis"));

		GUILayout.Label(AxisLabel("joystick button 0"));
		GUILayout.Label(AxisLabel("joystick button 1"));
		GUILayout.Label(AxisLabel("joystick button 2"));
		GUILayout.Label(AxisLabel("joystick button 3"));
		GUILayout.Label(AxisLabel("joystick button 4"));
		GUILayout.Label(AxisLabel("joystick button 5"));
		GUILayout.Label(AxisLabel("joystick button 6"));
		GUILayout.Label(AxisLabel("joystick button 7"));
		GUILayout.Label(AxisLabel("joystick button 8"));
		GUILayout.Label(AxisLabel("joystick button 9"));
		GUILayout.Label(AxisLabel("joystick button 10"));
		GUILayout.Label(AxisLabel("joystick button 11"));
		GUILayout.Label(AxisLabel("joystick button 12"));
		GUILayout.Label(AxisLabel("joystick button 13"));
	}

	string AxisLabel(string axisName) {
		return axisName + ":" + Input.GetAxisRaw(axisName);
	}

	string ButtonLabel(string buttonName) {
		return buttonName + ":" + Input.GetButton(buttonName);
	}
}
